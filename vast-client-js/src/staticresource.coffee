class VASTStaticResource
    constructor: ->
        @id = null
        @width = 0
        @height = 0
        @type = null
        @staticResource = null
        @companionClickThroughURLTemplate = null
        @trackingEvents = {}
        @apiFramework = null

module.exports = VASTStaticResource
