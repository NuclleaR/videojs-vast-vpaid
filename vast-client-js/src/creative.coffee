class VASTCreative
    constructor: ->
        @trackingEvents = {}

class VASTCreativeLinear extends VASTCreative
    constructor: ->
        super
        @type = "linear"
        @duration = 0
        @skipDelay = null
        @mediaFiles = []
        @videoClickThroughURLTemplate = null
        @videoClickTrackingURLTemplates = []

class VASTCreativeNonLinear extends VASTCreative
    # TODO
    constructor: ->
        super
        @type = "nonlinear"
        @duration = 0
        @staticResources = []
        @ClickThroughURLTemplate = null

class VASTCreativeCompanion extends VASTCreative
    constructor: ->
        @type = "companion"
        @variations = []
        @videoClickTrackingURLTemplates = []

module.exports =
    VASTCreativeLinear: VASTCreativeLinear
    VASTCreativeNonLinear: VASTCreativeNonLinear
    VASTCreativeCompanion: VASTCreativeCompanion
