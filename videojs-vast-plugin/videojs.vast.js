(function (window, vjs, vast, undefined) {
   'use strict';

   var extend = function (obj) {
         var arg, i, k;
         for (i = 1; i < arguments.length; i++) {
            arg = arguments[i];
            for (k in arg) {
               if (arg.hasOwnProperty(k)) {
                  obj[k] = arg[k];
               }
            }
         }
         return obj;
      },

      defaults = {
         // seconds before skip button shows, negative values to disable skip button altogether
         skip: 2, //TODO Change skip to 5s
         bitrate: 1000, //advised bitrate for VPAID ads
         viewMode: 'normal', //view mode for VPAID ads. Possible values: normal, thumbnail, fullscreen
         vpaidElement: undefined //html element used for vpaid ads
      },

      Vast = function (player, settings, VpaidOpts) {

         window.VpaidflashWrapper = {};

         window.VpaidflashWrapper.pause = function () {

         };
         window.VpaidflashWrapper.resume = function () {
            //console.error('resume');
            player.play();
            player.controls(true);
         };
         window.VpaidflashWrapper.adStarted = function () {
            //console.error('started')
         };
         window.VpaidflashWrapper.adError = function (e) {
            console.error('error', e);
            player.play();
            player.controls(true);
         };
         window.VpaidflashWrapper.adComplete = function () {
            //console.error('complete')
         };
         window.VpaidflashWrapper.startFlashWrapper = function () {
            //console.error('flash ready');
            //TODO
            //var XMLDoc = new XMLSerializer().serializeToString(window.VpaidflashWrapper.xml);
            // console.error(XMLDoc);
            player.pause();
            player.controls(false);
            this.config = {
               contentPauseRequestClosure: "VpaidflashWrapper.pause",
               contentResumeRequestClosure: "VpaidflashWrapper.resume",
               adStartedClosure: "VpaidflashWrapper.adStarted",
               adErrorClosure: "VpaidflashWrapper.adError",
               adCompleteClosure: "VpaidflashWrapper.adComplete",
               volume: 1,
               adLoadingTimeout: 10,
               vastResponse: (new XMLSerializer().serializeToString(window.VpaidflashWrapper.xml))
            };

            window.document['swfplayerWrapper'].showAd(JSON.stringify(VpaidflashWrapper.config))
         };

         // return vast plugin
         return {
            createSourceObjects: function (media_files) {
               console.log(media_files);
               var sourcesByFormat = {}, i, j, tech;
               var techOrder = player.options().techOrder;
               for (i = 0, j = techOrder.length; i < j; i++) {
                  var techName = techOrder[i].charAt(0).toUpperCase() + techOrder[i].slice(1);
                  tech = window.videojs[techName];

                  // Check if the current tech is defined before continuing
                  if (!tech) {
                     continue;
                  }

                  // Check if the browser supports this technology
                  if (tech.isSupported()) {
                     // Loop through each source object
                     for (var a = 0, b = media_files.length; a < b; a++) {
                        var media_file = media_files[a];
                        var source = {type: media_file.mimeType, src: media_file.fileURL};
                        // Check if source can be played with this technology
                        if (tech.canPlaySource(source)) {
                           if (sourcesByFormat[techOrder[i]] === undefined) {
                              sourcesByFormat[techOrder[i]] = [];
                           }
                           sourcesByFormat[techOrder[i]].push({
                              type: media_file.mimeType,
                              src: media_file.fileURL,
                              width: media_file.width,
                              height: media_file.height
                           });
                        } else if (tech.canPlaySource(source) == false && media_file.mimeType == "application/x-shockwave-flash") { /*TODO переписать условие*/
                           if (sourcesByFormat[techOrder[i]] === undefined) {
                              sourcesByFormat[techOrder[i]] = [];
                           }
                           sourcesByFormat[techOrder[i]].push({
                              type: media_file.mimeType,
                              src: media_file.fileURL,
                              width: media_file.width,
                              height: media_file.height,
                              apiFramework: media_file.apiFramework
                           });
                        }
                     }
                  }
               }
               // Create sources in preferred format order
               var sources = [];
               for (j = 0; j < techOrder.length; j++) {
                  tech = techOrder[j];
                  if (sourcesByFormat[tech] !== undefined) {
                     for (i = 0; i < sourcesByFormat[tech].length; i++) {
                        sources.push(sourcesByFormat[tech][i]);
                     }
                  }
               }
               return sources;
            },
            createStaticObject: function (creative) {
               //console.log(creative);
               var sources = [];
               for (var a = 0; a < creative.staticResources.length; a++) {
                  var source = {
                     type: creative.staticResources[a].mimeType,
                     src: creative.staticResources[a].fileURL,
                     width: creative.staticResources[a].width,
                     height: creative.staticResources[a].height,
                     clickThrough: creative.ClickThroughURLTemplate
                  };
                  sources.push(source);
               }
               return sources;
            },
            getContent: function () {
               // query vast url given in settings
               vast.client.get(settings.url, function (response) {
                  console.log(response);
                  if (response) {
                     player.vastResponse = response;
                     player.vastResponse.currentSequence = 1;
                     // we got a response, deal with it
                     for (var adIdx = 0; adIdx < response.ads.length; adIdx++) {
                        var ad = response.ads[adIdx];
                        player.vast.companion = undefined;
                        for (var creaIdx = 0; creaIdx < ad.creatives.length; creaIdx++) {
                           var creative = ad.creatives[creaIdx], foundCreative = false, foundCompanion = false;
                           if (creative.type === "linear" && !foundCreative) {
                              //console.log('linear', creative);
                              if (creative.mediaFiles.length) {
                                 //debugger;
                                 //player.vast.sources = player.vast.createSourceObjects(creative.mediaFiles);
                                 player.vast.setAdSources();
                                 if (!player.vast.sources.length) {
                                    player.trigger('adscanceled');
                                    return;
                                 }
                                 player.vastTracker = new vast.tracker(ad, creative);
                                 foundCreative = true;
                              }
                           } else if (creative.type === "companion" && !foundCompanion) {
                              //player.vast.companion = creative;
                              console.log(creative);
                              player.vast.setCompanion();
                              //player.vast.showCompanionResources(player.vast.companion);
                              foundCompanion = true;
                           } else if (creative.type === "nonlinear" && !foundCreative) {
                              //console.log("NonLinear");
                              if (creative.staticResources.length) {
                                 player.vast.nonlinearObjects = player.vast.createStaticObject(creative);
                                 if (!player.vast.nonlinearObjects.length) {
                                    player.trigger('adscanceled');
                                    return;
                                 }
                                 //player.vast.nonlinear = creative;
                                 player.vastTracker = new vast.tracker(ad, creative);

                                 foundCreative = true;
                              }
                           }
                        }

                        if (player.vastTracker) {
                           // vast tracker and content is ready to go, trigger event
                           player.trigger('vast-ready');
                           break;
                        } else {
                           // Inform ad server we can't find suitable media file for this ad
                           vast.util.track(ad.errorURLTemplates, {ERRORCODE: 403});
                        }
                     }
                  }

                  if (!player.vastTracker) {
                     // No pre-roll, start video
                     player.trigger('adscanceled');
                  }
               });
            },

            setAdSources: function () {
               player.vast.sources = player.vast.createSourceObjects(player.vastResponse.ads[player.vastResponse.currentSequence - 1].creatives[0].mediaFiles);
            },
            setCompanion: function () {
               player.vast.companion = player.vastResponse.ads[player.vastResponse.currentSequence - 1].creatives[1];
               player.vast.showCompanionResources(player.vast.companion);
            },
            removeCompanion: function () {
               var ads = document.querySelectorAll('.vast-companion-ad');
               for (var i = 0; i < ads.length; i++) {
                  var ad = ads[i];
                  ad.parentNode.removeChild(ad);
               }
            },

            setupEvents: function () {

               var errorOccurred = false,
                  canplayFn = function () {
                     player.vastTracker.load();
                  },
                  timeupdateFn = function () {
                     if (isNaN(player.vastTracker.assetDuration)) {
                        player.vastTracker.assetDuration = player.duration();
                     }
                     player.vastTracker.setProgress(player.currentTime());
                  },
                  pauseFn = function () {
                     player.vastTracker.setPaused(true);
                     player.one('play', function () {
                        player.vastTracker.setPaused(false);
                     });
                  },
                  errorFn = function () {
                     // Inform ad server we couldn't play the media file for this ad
                     vast.util.track(player.vastTracker.ad.errorURLTemplates, {ERRORCODE: 405});
                     errorOccurred = true;
                     player.trigger('ended');
                  },
                  mutedFn = function () {
                     //console.log(player.muted());
                     player.vastTracker.setMuted(player.muted());
                  },
                  fullscreenFn = function () {
                     player.vastTracker.setFullscreen(player.isFullscreen())
                  };

               player.on('canplay', canplayFn);
               player.on('timeupdate', timeupdateFn);
               player.on('pause', pauseFn);
               player.on('error', errorFn);
               player.on('volumechange', mutedFn);
               player.on('fullscreenchange', fullscreenFn);

               player.one('vast-preroll-removed', function () {
                  player.off('canplay', canplayFn);
                  player.off('timeupdate', timeupdateFn);
                  player.off('pause', pauseFn);
                  player.off('error', errorFn);
                  if (!errorOccurred) {
                     player.vastTracker.complete();
                  }
               });
            },

            preroll: function () {
               player.ads.startLinearAdMode();
               player.vast.showControls = player.controls();
               console.log(player.vast.showControls);
               if (player.vast.showControls) {
                  player.controls(false);
               }

               // load linear ad sources and start playing them
               player.src(player.vast.sources);

               var clickthrough;
               if (player.vastTracker.clickThroughURLTemplate) {
                  clickthrough = vast.util.resolveURLTemplates(
                     [player.vastTracker.clickThroughURLTemplate],
                     {
                        CACHEBUSTER: Math.round(Math.random() * 1.0e+10),
                        CONTENTPLAYHEAD: player.vastTracker.progressFormated()
                     }
                  )[0];
               }
               var blocker = window.document.createElement("a");
               blocker.className = "vast-blocker";
               blocker.href = clickthrough || "#";
               blocker.target = "_blank";
               blocker.onclick = function () {
                  if (player.paused()) {
                     player.play();
                     return false;
                  }
                  var clicktrackers = player.vastTracker.clickTrackingURLTemplate;
                  if (clicktrackers) {
                     player.vastTracker.trackURLs([clicktrackers]);
                  }
                  player.trigger("adclick");
               };
               player.vast.blocker = blocker;
               player.el().insertBefore(blocker, player.controlBar.el());

               var skipButton = window.document.createElement("div");
               skipButton.className = "vast-skip-button";
               if (settings.skip < 0) {
                  skipButton.style.display = "none";
               }
               player.vast.skipButton = skipButton;

               player.el().appendChild(skipButton);

               player.on("timeupdate", player.vast.timeupdate);

               skipButton.onclick = function (e) {
                  if ((' ' + player.vast.skipButton.className + ' ').indexOf(' enabled ') >= 0) {
                     player.vastTracker.skip();
                     player.vast.nextAdCall();
                  }
                  if (window.Event.prototype.stopPropagation !== undefined) {
                     e.stopPropagation();
                  } else {
                     return false;
                  }
               };

               player.vast.setupEvents();

               if (!(player.vastResponse.currentSequence < player.vastResponse.ads.length)) {
                  player.one('ended', player.vast.tearDown);
               }

               player.trigger('vast-preroll-ready');
            },

            nextAdCall: function () {
               if (!(player.vastResponse.currentSequence < player.vastResponse.ads.length)) {
                  player.vast.tearDown();
               } else {
                  player.trigger('ended');
               }
            },

            removeClicks: function () {
               player.vast.skipButton && player.vast.skipButton.parentNode.removeChild(player.vast.skipButton);
               player.vast.collapseButton && player.vast.collapseButton.parentNode.removeChild(player.vast.collapseButton);
               player.vast.expandButton && player.vast.expandButton.parentNode.removeChild(player.vast.expandButton);
               player.vast.blocker && player.vast.blocker.parentNode.removeChild(player.vast.blocker);
            },

            prerollVPAID: function () {

               VpaidOpts.vpaidTrackInterval = setInterval(player.vast.updateSeeker, 500);
               //player might be playing if video tags are different
               var blocker = window.document.createElement("div");
               blocker.innerHTML =
                  '<object id="swfplayerWrapper" width="640" height="400">\
                     <param name="movie" value="VASTPlayer.swf" />\
                     <param name="wmode" value="transparent">\
                     <embed\
                        src="VASTPlayer.swf"\
                        name="swfplayerWrapper" align="middle"\
                        play="true" loop="false" quality="high" allowScriptAccess="sameDomain"\
                        width="640" height="400" scale="exactfit"\
                        type="application/x-shockwave-flash">\
                        <param name="FlashVars" value="onReady=VpaidflashWrapper.startFlashWrapper&bgcolor=transparent"/>\
                     </embed>\
                  </object>';
               blocker.style.position = 'absolute';
               blocker.style.top = '0px';
               blocker.style.left = '0px';

               player.vast.blocker = blocker;
               // player.el().appendChild(blocker)
               player.el().insertBefore(blocker, player.controlBar.el());
            },
            // Companion Ads blocks
            showCompanionResources: function (creative) {
               // iterate through companions and add them to body
               var companionContent, param;

               for (var i = 0; i < creative.variations.length; i++) {
                  var companionBlock = document.createElement('div');
                  //document.body.appendChild(companionBlock);

                  // iterate through possible placeholders
                  for (var placeholderWidth in player.companionPlaceholders) {
                     // append creative to block that fits size of placeholder
                     var sizeDiff = Math.abs(parseInt(placeholderWidth) - creative.variations[i].width);
                     if (creative.variations[i].width <= parseInt(placeholderWidth) && sizeDiff < 50) {
                        var placeholder = document.getElementById(player.companionPlaceholders[placeholderWidth]);
                        placeholder.appendChild(companionBlock);
                     } //else {console.log('creative > container. Creative:', creative.variations[i].width, 'Container', placeholderWidth);}
                  }

                  console.log(creative.variations[i].type);
                  //"image/gif".match(/(.*)\//)[1]
                  switch (creative.variations[i].type.split('/')[0]) {
                     case "IFrame":
                        var iFrame = document.createElement('iframe');
                        iFrame.src = creative.variations[i].staticResource;
                        iFrame.width = creative.variations[i].width;
                        iFrame.height = creative.variations[i].height;
                        iFrame.frameBorder = "0";
                        iFrame.scrolling = "no";
                        companionBlock.appendChild(iFrame);
                        break;
                     case "HTML":
                        companionBlock.innerHTML = creative.variations[i].staticResource;
                        break;
                     case "image":
                        var companionClickThroughLink = document.createElement('a');
                        companionClickThroughLink.href = creative.variations[i].companionClickThroughURLTemplate;
                        companionClickThroughLink.target = '_blank';
                        companionContent = document.createElement('img');
                        companionContent.src = creative.variations[i].staticResource;
                        companionClickThroughLink.appendChild(companionContent);
                        companionClickThroughLink && companionBlock.appendChild(companionClickThroughLink);
                        break;
                     case "application":
                        companionContent = document.createElement('object');
                        param = document.createElement('param');
                        param.name = 'FlashVars';
                        param.value = "clickTag=" + encodeURIComponent(creative.variations[i].companionClickThroughURLTemplate);
                        companionContent.appendChild(param);
                        //companionContent.style.pointerEvents = "none";
                        companionContent.setAttribute("type", "application/x-shockwave-flash");
                        companionContent.setAttribute("data", creative.variations[i].staticResource);
                        companionBlock.appendChild(companionContent);
                  }

                  companionBlock.className = 'vast-companion-ad';
                  companionBlock.style.width = creative.variations[i].width;

                  if (companionContent) {
                     companionContent.className = 'vast-companion-content';
                     companionContent.width = creative.variations[i].width;
                     companionContent.height = creative.variations[i].height;
                  }
               }
            },

            // Non linear overlay
            showStaticResource: function () {
               //console.log("Show NonLinear");
               player.ads.startNonLinearAdMode();
               var clickthrough;
               console.log(player.vastTracker);
               if (player.vastTracker.clickThroughURLTemplate) {
                  clickthrough = vast.util.resolveURLTemplates(
                     [player.vastTracker.clickThroughURLTemplate],
                     {
                        CACHEBUSTER: Math.round(Math.random() * 1.0e+10),
                        CONTENTPLAYHEAD: player.vastTracker.progressFormated()
                     }
                  )[0];
               } else {
                  clickthrough = player.vastTracker.creative.ClickThroughURLTemplate
               }

               var overlay = window.document.createElement("a");
               overlay.className = "vast-overlay";
               overlay.href = clickthrough || "#";
               overlay.target = "_blank";
               overlay.style.width = player.vast.nonlinearObjects[0].width + 'px';
               overlay.style.height = player.vast.nonlinearObjects[0].height + 'px';
               overlay.onclick = function () {
                  var clicktrackers = player.vastTracker.clickTrackingURLTemplate;
                  if (clicktrackers) {
                     player.vastTracker.trackURLs([clicktrackers]);
                  }
                  player.trigger("adclick");
               };
               player.vast.blocker = overlay;
               player.el().insertBefore(overlay, player.controlBar.el());

               if (player.vast.nonlinearObjects[0].type.indexOf("image") != -1) {  /*todo array images impl*/
                  overlay.style.backgroundImage = "url(" + player.vast.nonlinearObjects[0].src + ")";
               }
               var collapseButton = document.createElement("div");
               var expandButton = document.createElement("div");

               collapseButton.className = "vast-button collapse";
               expandButton.className = "vast-button expand";

               player.vast.collapseButton = collapseButton;
               player.vast.collapseButton.style.display = "block";
               player.vast.collapseButton.innerHTML = "&times;";

               player.vast.expandButton = expandButton;
               player.vast.expandButton.innerHTML = "+";

               player.el().appendChild(expandButton);

               player.vast.blocker.appendChild(collapseButton);

               player.on("timeupdate", player.vast.showingTime);

               collapseButton.onclick = function (e) {
                  e.preventDefault();
                  player.off('timeupdate', player.vast.showingTime);
                  player.vastTracker.setCollapse(false);
                  player.vast.collapseExpandBanner(false);
               };
               expandButton.onclick = function (e) {
                  player.vastTracker.setCollapse(true);
                  player.vast.collapseExpandBanner(true);
               };

               player.vast.setupEvents();
               player.vastTracker.load();
               player.one('ended', player.vast.removeClicks);
               player.one('ended', player.vast.tearDown);
               player.trigger('vast-nonlinear-ready');
            },

            collapseExpandBanner: function (cond) {
               player.vast.blocker && (player.vast.blocker.style.display = cond ? "block" : "none");
               player.vast.expandButton && (player.vast.expandButton.style.display = cond ? "none" : "block");
            },
            showingTime: function () {
               player.loadingSpinner.el().style.display = "none";
               var timeLeft = Math.ceil(player.vastTracker.creative.minSuggestedDuration - player.currentTime());
               //console.log(timeLeft);
               if (timeLeft == -1) {
                  player.off('timeupdate', player.vast.showingTime);
                  return;
               }
               if (timeLeft <= 0) {
                  player.vast.collapseExpandBanner(false);
               }
            },

            tearDown: function () {
               // remove preroll buttons
               player.vast.skipButton && player.vast.skipButton.parentNode.removeChild(player.vast.skipButton);
               player.vast.collapseButton && player.vast.collapseButton.parentNode.removeChild(player.vast.collapseButton);
               player.vast.expandButton && player.vast.expandButton.parentNode.removeChild(player.vast.expandButton);
               player.vast.blocker && player.vast.blocker.parentNode.removeChild(player.vast.blocker);

               // remove vast-specific events
               player.off('timeupdate', player.vast.timeupdate);
               player.off('ended', player.vast.tearDown);

               // end ad mode
               if (player.vastTracker.linear) {
                  player.ads.endLinearAdMode();
               } else if (player.vastTracker.nonlinear) {
                  player.ads.endNonLinearAdMode();
               }
               player.vast.showControls = true;
               // show player controls for video
               if (player.vast.showControls) {
                  player.controls(true);
               }

               player.trigger('vast-preroll-removed');
            },

            timeupdate: function () {
               player.loadingSpinner.el().style.display = "none";
               var timeLeft = Math.ceil(settings.skip - player.currentTime());
               if (timeLeft > 0) {
                  if (player.vastTracker.linear) {
                     player.vast.skipButton.innerHTML = "Skip in " + timeLeft + "...";
                  }
               } else {
                  if (player.vastTracker.linear) {
                     if ((' ' + player.vast.skipButton.className + ' ').indexOf(' enabled ') === -1) {
                        player.vast.skipButton.className += " enabled";
                        player.vast.skipButton.innerHTML = "Skip";
                     }
                  }
               }
            }
         };
      },

      vastPlugin = function (options) {
         var VpaidOpts;
         var vpaidObj;
         var player = this;
         var settings = extend({}, defaults, options || {});
         var vpaidListeners = {}, vpaidIFrame = null, vpaidPlayer = null, vpaidTrackInterval = -1, vpaidSeeker;
         VpaidOpts = {
            vpaidObj: vpaidObj,
            vpaidListeners: vpaidListeners,
            vpaidIFrame: vpaidIFrame,
            vpaidPlayer: vpaidPlayer,
            vpaidTrackInterval: vpaidTrackInterval,
            vpaidSeeker: vpaidSeeker
         };
         // check that we have the ads plugin
         if (player.ads === undefined) {
            window.console.error('vast video plugin requires videojs-contrib-ads, vast plugin not initialized');
            return null;
         }
         // set up vast plugin, then set up events here
         player.vast = new Vast(player, settings, VpaidOpts);
         this.companionPlaceholders = settings.companionPlaceholders;
         player.on('vast-ready', function () {
            // vast is prepared with content, set up ads and trigger ready function
            if (player.vastTracker.nonlinear) {
               player.trigger('nonlinearadsready');
            } else {
               player.trigger('adsready');
            }
         });

         player.on('vast-preroll-ready', function () {
            // start playing preroll, note: this should happen this way no matter what, even if autoplay
            //  has been disabled since the preroll function shouldn't run until the user/autoplay has
            //  caused the main video to trigger this preroll function
            player.play();
         });
         player.on('vast-nonlinear-ready', function () {
            player.play();
         });

         player.on('vast-preroll-removed', function () {
            // preroll done or removed, start playing the actual video
            player.play();
         });

         player.on('contentupdate', function () {
            // videojs-ads triggers this when src changes
            player.vast.getContent(settings.url);
         });

         player.on('readyforpreroll', function () {
            // if we don't have a vast url, just bail out
            if (!settings.url) {
               player.trigger('adscanceled');
               return null;
            }
            if (player.vast.sources[0].apiFramework && player.vast.sources[0].apiFramework == 'VPAID') {
               player.vast.prerollVPAID();

            } else {
               // set up and start playing preroll
               player.vast.preroll();
            }

         });
         player.on('showoverlay', function () {
            player.vast.showStaticResource();
         });

         // make an ads request immediately so we're ready when the viewer hits "play"
         if (player.currentSrc()) {
            player.vast.getContent(settings.url);
         }

         // return player to allow this plugin to be chained
         return player;
      };

   vjs.plugin('vast', vastPlugin);

}(window, videojs, DMVAST));